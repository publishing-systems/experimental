/* Copyright (C) 2021  Stephan Kreutzer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file
 * @author Stephan Kreutzer
 * @since 2021-07-19
 */


import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;


public class Text
{
    public static void main(String args[])
    {
        Text instance = new Text();
        instance.run(args);
    }

    public Text()
    {

    }

    public int run(String args[])
    {
        if (args.length < 1)
        {
            System.exit(1);
        }

        File inputFile = new File(args[0]);

        try
        {
            inputFile = inputFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }

        if (inputFile.exists() != true)
        {
            System.exit(1);
        }

        if (inputFile.isFile() != true)
        {
            System.exit(1);
        }

        if (inputFile.canRead() != true)
        {
            System.exit(1);
        }


        Map<Long, Word> words = new HashMap<Long, Word>();
        long wordCount = 0L;

        try
        {
            BufferedReader reader = new BufferedReader(
                                    new InputStreamReader(
                                    new FileInputStream(inputFile),
                                    "UTF-8"));

            do
            {
                int character = reader.read();

                if (character < 0)
                {
                    break;
                }

                if (Character.isWhitespace((char)character) == true)
                {
                    do
                    {
                        character = reader.read();

                        if (character < 0)
                        {
                            break;
                        }

                    } while (Character.isWhitespace((char)character) == true);

                    if (character >= 0)
                    {
                        wordCount += 1L;
                        words.put(wordCount, new Word(wordCount));
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    if (wordCount == 0L)
                    {
                        wordCount += 1L;
                        words.put(wordCount, new Word(wordCount));
                    }
                }

                Map<String, Long> characters = words.get(wordCount).GetCharacters();

                /** @todo Always lower-case? */
                String characterString = new String();
                characterString += (char)character;

                if (characters.containsKey(characterString) != true)
                {
                    characters.put(characterString, 0L);
                }

                characters.put(characterString, characters.get(characterString) + 1L);

                words.get(wordCount).GetString().append(characterString);

            } while (true);

            reader.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }

        /*
        for (Map.Entry<Long, Word> word : words.entrySet())
        {
            for (Map.Entry<String, Long> character : word.getValue().GetCharacters().entrySet())
            {
                System.out.println(character.getKey() + ": " + character.getValue());
            }

            System.out.print("\n");
        }
        */

        Iterator<Map.Entry<Long, Word>> iterLhs = words.entrySet().iterator();

        while (iterLhs.hasNext() == true)
        {
            Map.Entry<Long, Word> wordLhs = iterLhs.next();

            System.out.println("\"" + wordLhs.getValue().GetStringCurrent() + "\":");

            Iterator<Map.Entry<Long, Word>> iterRhs = words.entrySet().iterator();

            while (iterRhs.hasNext() == true)
            {
                Map.Entry<Long, Word> wordRhs = iterRhs.next();

                if (wordLhs.getKey() == wordRhs.getKey())
                {
                    continue;
                }

                int matches = 0;

                for (Map.Entry<String, Long> characterLhs : wordLhs.getValue().GetCharacters().entrySet())
                {
                    for (Map.Entry<String, Long> characterRhs : wordRhs.getValue().GetCharacters().entrySet())
                    {
                        if (characterLhs.getKey().equals(characterRhs.getKey()) == true)
                        {
                            matches += 1;
                        }
                    }
                }


                if (matches > 0)
                {
                    /*
                    int lengthDiff = wordLhs.getValue().GetStringCurrent().length() - wordRhs.getValue().GetStringCurrent().length();
                    int matchDiff = wordLhs.getValue().GetStringCurrent().length() - matches;

                    int totalDiff = lengthDiff + matchDiff;

                    if (lengthDiff == 0)
                    {
                        System.out.println(wordRhs.getValue().GetStringCurrent() + ": " + totalDiff);
                        //System.out.println(wordRhs.getValue().GetStringCurrent() + ": " + matches + ", " + lengthDiff);
                    }
                    */
                    /*
                    for (Map.Entry<String, Long> character : wordRhs.getValue().entrySet())
                    {
                        System.out.print(character.getKey());
                    }

                    System.out.print(": " + matches + "\n");
                    */
                }

                if (matches > 0)
                {
                    List<Integer> matchList = new ArrayList<Integer>();
                    int matchCount = 0;
                    int matchCountHighest = 0;

                    String needle = null;
                    String haystack = null;

                    if (wordLhs.getValue().GetStringCurrent().length() >= wordRhs.getValue().GetStringCurrent().length())
                    {
                        needle = wordLhs.getValue().GetStringCurrent();
                        haystack = wordRhs.getValue().GetStringCurrent();
                    }
                    else
                    {
                        needle = wordRhs.getValue().GetStringCurrent();
                        haystack = wordLhs.getValue().GetStringCurrent();
                    }

                    {
                        int posNeedle = 0;

                        for (int i = 0, max = haystack.length(); i < max; i++)
                        {
                            if (haystack.charAt(i) == needle.charAt(posNeedle))
                            {
                                posNeedle += 1;
                                matchCount += 1;
                            }
                            else
                            {
                                if (matchCount > 0)
                                {
                                    matchList.add(matchCount);

                                    if (matchCount > matchCountHighest)
                                    {
                                        matchCountHighest = matchCount;
                                    }
                                }

                                matchCount = 0;
                                posNeedle = 0;
                            }
                        }
                    }

                    if (matchCount > 0)
                    {
                        matchList.add(matchCount);

                        if (matchCount > matchCountHighest)
                        {
                            matchCountHighest = matchCount;
                        }
                    }

                    if (matchList.size() > 0)
                    {
                        System.out.print(wordRhs.getValue().GetStringCurrent() + ": ");
                        System.out.print(matchCountHighest);

                        /*
                        for (int i = 0, max = matchList.size(); i < max; i++)
                        {
                            System.out.print(matchList.get(i) + ", ");
                        }
                        */

                        System.out.print("\n");
                    }
                }
            }

            System.out.print("\n\n");
        }

        return 0;
    }
}
