<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-manager-1.
 *
 * entry-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/entry_management.inc.php
 * @author Stephan Kreutzer
 * @since 2024-01-24
 */

require_once(dirname(__FILE__)."/database.inc.php");


function getEntryByUserId($userId)
{
    $entry = Database::Get()->query("SELECT `id`,\n".
                                    "    `name`\n".
                                    "FROM `".Database::GetPrefix()."entry`\n".
                                    "WHERE `id_user`=".((int)$userId)."\n");

    if ($entry == false)
    {
        return -1;
    }

    $entry = Database::GetResultAssoc($entry);

    return $entry;
}

function insertEntry($name)
{
    $result = Database::Get()->query("INSERT INTO `".Database::GetPrefix()."entry` (`id`,\n".
                                     "    `name`,\n".
                                     "    `id_user`)\n".
                                     "VALUES (NULL,\n".
                                     "    \"".Database::Get()->real_escape_string($name)."\",\n".
                                     "    ".$_SESSION['user_id'].")\n");

    if ($result !== true)
    {
        return -1;
    }

    if (Database::Get()->insert_id === NULL)
    {
        return -1;
    }

    return Database::Get()->insert_id;
}

function updateEntry($id, $name)
{
    $result = Database::Get()->query("UPDATE `".Database::GetPrefix()."entry`\n".
                                     "SET `name`=\"".Database::Get()->real_escape_string($name)."\"\n".
                                     "WHERE `id`=".((int)$id)."\n");

    if ($result !== true)
    {
        return false;
    }

    return true;
}

function deleteEntry($id)
{
    $result = Database::Get()->query("DELETE FROM `".Database::GetPrefix()."entry`\n".
                                     "WHERE `id`=".((int)$id)."\n");

    if ($result !== true)
    {
        return false;
    }

    return true;
}


?>
