<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-manager-1.
 *
 * entry-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/database.inc.php
 * @author Stephan Kreutzer
 * @since 2023-01-24
 */


class Database
{
    static public function Get()
    {
        if (self::$db == NULL)
        {
            self::$dbTableNamePrefix = "";

            // With MYSQLI_REPORT_STRICT, it'll throw exceptions!
            //mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            mysqli_report(MYSQLI_REPORT_ERROR);

            self::$db = new mysqli("localhost", "root", "", "entry_manager");

            if (self::$db->connect_errno != 0)
            {
                http_response_code(500);
                //echo self::$db->connect_error;
                exit(1);
            }

            if (self::$db->set_charset("utf8") !== true)
            {

            }

            if (self::$db->errno != 0)
            {
                http_response_code(500);
                //echo self::$db->error;
                exit(1);
            }
        }

        return self::$db;
    }

    static public function GetPrefix()
    {
        return self::$dbTableNamePrefix;
    }

    static public function GetResultAssoc($resultHandle)
    {
        $result = array();

        while (true)
        {
            $temp = mysqli_fetch_assoc($resultHandle);

            if ($temp !== null)
            {
                $result[] = $temp;
            }
            else
            {
                break;
            }
        }

        mysqli_free_result($resultHandle);

        return $result;
    }

    private function __construct()
    {

    }

    public function __destruct()
    {

    }

    protected static $db = NULL;
    protected static $dbTableNamePrefix = "";
}


?>
