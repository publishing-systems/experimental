<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-manager-1.
 *
 * entry-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/user_management.inc.php
 * @author Stephan Kreutzer
 * @since 2024-01-24
 */

require_once(dirname(__FILE__)."/user_defines.inc.php");
require_once(dirname(__FILE__)."/database.inc.php");


function getUserByName($name)
{
    $user = Database::Get()->query("SELECT `id`,\n".
                                   "    `e_mail`,\n".
                                   "    `password`,\n".
                                   "    `role`\n".
                                   "FROM `".Database::GetPrefix()."user`\n".
                                   "WHERE `name` LIKE \"".Database::Get()->real_escape_string($name)."\"\n");

    if ($user == false)
    {
        return -1;
    }

    $user = Database::GetResultAssoc($user);

    return $user;
}


?>
