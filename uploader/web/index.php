<?php
/* Copyright (C) 2012-2024 Stephan Kreutzer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/index.php
 * @brief Page for uploading new files.
 * @author Stephan Kreutzer
 * @since 2019-06-22
 */


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "  PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "  \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Upload</title>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div>\n".
     "      <div>\n";

if (isset($_POST['submit']) !== true)
{
    echo "        <form enctype=\"multipart/form-data\" action=\"index.php\" method=\"POST\">\n".
         "          <fieldset>\n".
         "            <input name=\"file\" type=\"file\" /><br />\n".
         "            <input type=\"submit\" name=\"submit\" value=\"upload\" /><br />\n".
         "          </fieldset>\n".
         "        </form>\n";
}
else
{
    $validFile = true;

    if (isset($_FILES['file']) !== true)
    {
        echo "        <p>Submission erroneous.</p>\n";
        $validFile = false;
    }

    if ($validFile === true)
    {
        if ($_FILES['file']['error'] != 0)
        {
            echo "        <p>Error occurred with number ".$_FILES['file']['error']."</p>\n";
            $validFile = false;
        }
    }

    if ($validFile === true)
    {
        /**
         * @todo In contrast to the hard upload_max_filesize and post_max_size, this is a
         *     soft-limit and needs to be made configurable.
         */
        /*
        if ($_FILES['file']['size'] > 10486000)
        {
            echo "        <p>File has more bytes than 10486000</p>\n";
            $validFile = false;
        }
        */
    }

    $id = null;

    if ($validFile === true)
    {
        for ($i = 0; $i < 30; $i++)
        {
            $tempId = md5(uniqid(rand(), true));

            if (file_exists("./uploads/".$tempId) === false)
            {
                $id = $tempId;
                break;
            }
        }

        if ($id == null)
        {
            echo "        <p>The file has been successfully uploaded, but can’t be stored.</p>\n";
            $validFile = false;
        }
    }

    if ($validFile === true)
    {
        if (move_uploaded_file($_FILES['file']['tmp_name'], "./uploads/".$id) === true)
        {
            echo "        <p>Upload successful.</p>\n";
        }
        else
        {
            echo "        <p>The file has been successfully uploaded, but couldn’t be stored.</p>\n";
        }
    }
}

echo "      </div>\n".
     "      <div>\n".
     "        <p>\n".
     "          Copyright (C) 2012-2024 Stephan Kreutzer. This program is licensed under the <a href=\"https://www.gnu.org/licenses/agpl-3.0.html\">GNU Affero General Public License 3 + any later version</a>, see <a href=\"https://gitlab.com/publishing-systems/experimental/-/tree/master/uploader\">https://gitlab.com/publishing-systems/experimental/-/tree/master/uploader</a>.\n".
     "        </p>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n";


?>
