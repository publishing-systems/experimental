<?php
/* Copyright (C) 2024 Stephan Kreutzer
 *
 * This file is part of graph-world.
 *
 * graph-world is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * graph-world is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with graph-world. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/api.php
 * @details Stupid JSON! HTTP parameters contain names with minus notation,
 *     but JSON output needs to supply names in camel case, because in
 *     JavaScript, minus would be the arithmetic operation and would require
 *     special notation to access these names :-(
 * @attention This is not hypermedia :-(
 * @author Stephan Kreutzer
 * @since 2023-05-13
 */



$authentication = null;

{
    $headers = apache_request_headers();

    if ($headers === false)
    {
        http_response_code(500);
        exit(-1);
    }

    $authorizationHeaderName = null;

    foreach ($headers as $headerName => $value)
    {
        if (strtolower($headerName) === "authorization")
        {
            $authorizationHeaderName = $headerName;
            break;
        }
    }

    if ($authorizationHeaderName !== null)
    {
        if (strpos($headers[$authorizationHeaderName], "Basic ") !== 0)
        {
            http_response_code(401);
            header("WWW-Authenticate: Basic realm=\"Authentication\", charset=\"UTF-8\"");
            exit(0);
        }

        $authorizationHeaderValue = substr($headers[$authorizationHeaderName], strlen("Basic "));
        $authorizationHeaderValue = base64_decode($authorizationHeaderValue, true);

        if ($authorizationHeaderValue === false)
        {
            http_response_code(400);
            exit(0);
        }

        $colonPos = mb_strpos($authorizationHeaderValue, ":", 0, "UTF-8");

        if ($colonPos === false)
        {
            http_response_code(400);
            exit(0);
        }

        $userName = mb_substr($authorizationHeaderValue, 0, $colonPos, "UTF-8");
        $userPassword = mb_substr($authorizationHeaderValue, $colonPos + 1, null, "UTF-8");


        require_once("../libraries/user_management.inc.php");

        $user = getUserByName($userName);

        if (is_array($user) !== true)
        {
            http_response_code(500);
            exit(0);
        }

        if (count($user) !== 1)
        {
            http_response_code(401);
            exit(0);
        }

        $user = $user[0];

        if ($user["password"] !== hash("sha512", $user["salt"].$userPassword))
        {
            http_response_code(401);
            exit(0);
        }

        /** @todo Set info in $authentication for later access control list checks. */
    }
    else
    {
        /** @todo Check with access control list if public/unauthenticated
          * actions are permitted. */
    }
}



$object = null;

/** @todo Change from $_GET to $_POST? Or actually check for $_SERVER["REQUEST_METHOD"]
  * (with $_GET parameters as partial ifdef-debug override?) and use a combination
  * of $_GET and $_POST. */
if (isset($_GET["object"]) !== true)
{
    http_response_code(400);
    exit(-1);
}

$object = $_GET["object"];

$action = null;

if (isset($_GET["action"]) === true)
{
    $action = $_GET["action"];
}
else
{
    $action = "read";
}


if ($object == "table")
{
    // if ($_SERVER['REQUEST_METHOD'] === "POST")
    if ($action == "create")
    {
        if (isset($_GET["caption"]) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        $tableNumber = -1;

        {
            $tables = null;

            if (file_exists("../definitions/table.json") === true)
            {
                $tables = @file_get_contents("../definitions/table.json");

                if ($tables === false)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $tables = json_decode($tables, false);

                if ($tables === null)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $tableNumber = count($tables);

                foreach ($tables as $table)
                {
                    if (((int)$table->id) > $tableNumber)
                    {
                        $tableNumber = (int)$table->id;
                    }
                }

                $tableNumber += 1;

                /** @todo Maybe support versioned/history? */
                if (@copy("../definitions/table.json", "../definitions/table.backup.json") !== true)
                {
                    http_response_code(500);
                    exit(-1);
                }
            }
            else
            {
                $tableNumber = 1;
                $tables = array();
            }

            $tables[] = array("id" => $tableNumber, "caption" => $_GET["caption"]);

            if (@file_put_contents("../definitions/table.json", json_encode($tables)) === false)
            {
                http_response_code(500);
                exit(-1);
            }
        }


        require_once("../libraries/database.inc.php");

        if (Database::Get()->IsConnected() !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        /** @todo Increment table_n! And make Execute()? */
        $success = Database::Get()->ExecuteUnsecure("CREATE TABLE IF NOT EXISTS `".Database::Get()->GetPrefix()."table_".((int)$tableNumber)."` (\n".
                                                    "    `id` INT(11) NOT NULL AUTO_INCREMENT,\n".
                                                    "    PRIMARY KEY (`id`)\n".
                                                    ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin");

        if ($success !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        http_response_code(201);
        header("Content-Type: application/json");
        echo "{\"tableId\":".((int)$tableNumber)."}";

        exit(0);
    }
    else if ($action == "read")
    {
        /** @todo Maybe the installer could create a meta-table (number 1) +
          * column definition which then could be queried like any other,
          * containing the list of available other tables and here outputting
          * it in the same data scheme as other responses. */

        if (isset($_GET["table-id"]) !== true)
        {
            $tables = null;

            if (file_exists("../definitions/table.json") === true)
            {
                $tables = @file_get_contents("../definitions/table.json");

                if ($tables === false)
                {
                    http_response_code(500);
                    exit(-1);
                }
            }
            else
            {
                $tables = "[]";
            }

            http_response_code(200);
            header("Content-Type: application/json");
            echo $tables;

            exit(0);

            /*
            $tables = null;

            if (file_exists("../definitions/table.json") === true)
            {
                $tables = @file_get_contents("../definitions/table.json");

                if ($tables === false)
                {

                }

                $tables = json_decode($tables, false);

                if ($tables === null)
                {

                }
            }
            else
            {
                $tables = array();
            }

            header("Content-Type: application/json");
            echo "[";

            $first = true;

            foreach ($tables as $table)
            {
                if ($first === true)
                {
                    $first = false;
                }
                else
                {
                    echo ",";
                }

                echo "{\"id\":".((int)$table->id).",".
                       \"caption\":".json_encode($table->caption)."}";
            }

            echo "]";
            */
        }
        else
        {
            $tableId = (int)$_GET["table-id"];

            if (file_exists("../definitions/table.json") !== true)
            {
                http_response_code(404);
                exit(1);
            }

            $tables = @file_get_contents("../definitions/table.json");

            if ($tables === false)
            {
                http_response_code(500);
                exit(-1);
            }

            $tables = json_decode($tables, false);

            if ($tables === null)
            {
                http_response_code(500);
                exit(-1);
            }

            $found = false;

            foreach ($tables as $table)
            {
                if ($table->id == $tableId)
                {
                    $found = true;
                    break;
                }
            }

            if ($found != true)
            {
                http_response_code(404);
                exit(1);
            }


            require_once("../libraries/column_type_defines.inc.php");

            $columns = null;

            if (file_exists("../definitions/table_".$tableId."_column.json") === true)
            {
                $columnsTemp = @file_get_contents("../definitions/table_".$tableId."_column.json");

                if ($columnsTemp === false)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $columnsTemp = json_decode($columnsTemp, false);

                if ($columnsTemp === null)
                {
                    http_response_code(500);
                    exit(-1);
                }

                foreach ($columnsTemp as $column)
                {
                    if (isset($columns[(int)$column->id]) === true)
                    {
                        http_response_code(500);
                        exit(-1);
                    }

                    if (in_array((int)$column->type, COLUMN_TYPES) !== true)
                    {
                        http_response_code(500);
                        exit(-1);
                    }

                    /** @todo Resolve type? */

                    $columns[(int)$column->id] = $column;
                }
            }
            else
            {
                http_response_code(500);
                exit(-1);
            }


            require_once("../libraries/database.inc.php");

            $sql = "SELECT `id`";

            foreach ($columns as $column)
            {
                $sql .= ",\n".
                        "    `column_".((int)$column->id)."`";
            }

            $sql .= "\n".
                    "FROM `".Database::Get()->GetPrefix()."table_".((int)$tableId)."`\n".
                    "WHERE 1\n".
                    "ORDER BY `id` ASC";

            if (Database::Get()->IsConnected() !== true)
            {
                http_response_code(500);
                exit(-1);
            }

            $records = Database::Get()->QueryUnsecure($sql);

            if (is_array($records) !== true)
            {
                http_response_code(500);
                exit(-1);
            }


            http_response_code(200);
            header("Content-Type: application/json");

            echo "{\"columns\":[";

            {
                $first = true;

                foreach ($columns as $column)
                {
                    if ($first == true)
                    {
                        $first = false;
                    }
                    else
                    {
                        echo ",";
                    }

                    echo "{\"columnId\":".((int)$column->id).",".
                          "\"caption\":".json_encode($column->caption).
                          /** @todo Map the type. */
                         "}";
                }
            }

            echo "],\"rows\":[";

            $lastRecordId = -1;

            for ($i = 0, $max = count($records); $i < $max; $i++)
            {
                if ($i > 0)
                {
                    echo ",";
                }

                echo "{\"recordId\":".((int)$records[$i]["id"]);

                foreach ($columns as $columnId => $column)
                {
                    echo ",\"column_".((int)$columnId)."\":";

                    switch ((int)$column->type)
                    {
                    case COLUMN_TYPE_VARCHAR:
                        echo json_encode($records[$i]["column_".((int)$columnId)]);
                        break;
                    case COLUMN_TYPE_BOOLEAN:
                        if ($records[$i]["column_".((int)$columnId)] === 0)
                        {
                            echo "false";
                        }
                        /**
                         * @todo This is not effective, as database NULL is not explicitly
                         * made available in the result set.
                         */
                        else if ($records[$i]["column_".((int)$columnId)] === NULL)
                        {
                            echo "null";
                        }
                        else
                        {
                            echo "true";
                        }
                        break;
                    case COLUMN_TYPE_INTEGER:
                        echo (int)$records[$i]["column_".((int)$columnId)];
                        break;
                    default:
                        // Too late to send the HTTP response status code.
                        throw new Exception("Unknown datatype.");
                    }
                }

                echo "}";
            }

            echo "]}";

            exit(0);
        }
    }
    else if ($action == "update")
    {
        http_response_code(400);
        exit(1);
    }
    else if ($action == "delete")
    {
        http_response_code(400);
        exit(1);
    }
    else
    {
        /** @todo If this were HTTP verbs, there's HTTP 405. */
        http_response_code(400);
        exit(1);
    }
}
else if ($object == "column")
{
    if ($action == "create")
    {
        if (isset($_GET["table-id"]) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        $tableId = (int)$_GET["table-id"];

        if (isset($_GET["caption"]) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        if (isset($_GET["type"]) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        $type = (int)$_GET["type"];

        require_once("../libraries/column_type_defines.inc.php");

        if (in_array($type, COLUMN_TYPES) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        $columnNumber = -1;

        {
            $columns = null;

            if (file_exists("../definitions/table_".$tableId."_column.json") === true)
            {
                $columns = @file_get_contents("../definitions/table_".$tableId."_column.json");

                if ($columns === false)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $columns = json_decode($columns, false);

                if ($columns === null)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $columnNumber = count($columns);

                foreach ($columns as $column)
                {
                    if (((int)$column->id) > $columnNumber)
                    {
                        $columnNumber = (int)$column->id;
                    }
                }

                $columnNumber += 1;

                /** @todo Maybe support versioned/history? */
                if (@copy("../definitions/table_".$tableId."_column.json", "../definitions/table_".$tableId."_column.backup.json") !== true)
                {
                    http_response_code(500);
                    exit(-1);
                }
            }
            else
            {
                if (file_exists("../definitions/table.json") !== true)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $tables = @file_get_contents("../definitions/table.json");

                if ($tables === false)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $tables = json_decode($tables, false);

                if ($tables === null)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $found = false;

                foreach ($tables as $table)
                {
                    if ($table->id == $tableId)
                    {
                        $found = true;
                        break;
                    }
                }

                if ($found != true)
                {
                    http_response_code(404);
                    exit(1);
                }

                $columnNumber = 1;

                $columns = array();
            }

            $columns[] = array("id" => $columnNumber, "caption" => $_GET["caption"], "type" => $type, "datetime" => gmdate("Y-m-d\TH:i:s\Z", time()));

            if (@file_put_contents("../definitions/table_".$tableId."_column.json", json_encode($columns)) === false)
            {
                http_response_code(500);
                exit(-1);
            }
        }


        require_once("../libraries/database.inc.php");

        $sql = "ALTER TABLE `".Database::Get()->GetPrefix()."table_".((int)$tableId)."`\n".
               "ADD `column_".$columnNumber."`";

        switch ($type)
        {
        case COLUMN_TYPE_VARCHAR:
            /** @todo Maybe not hardcode this, but source it
              * from the data type definition? */
            $sql .= " VARCHAR(255)";
            break;
        case COLUMN_TYPE_BOOLEAN:
            $sql .= " BOOLEAN";
            break;
        case COLUMN_TYPE_INTEGER:
            $sql .= " INT(11)";
            break;
        default:
            // There was already a check before, so if default here,
            // this is because type wasn't mapped in the code here.
            http_response_code(500);
            exit(-1);
        };

        if (Database::Get()->IsConnected() !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        if (Database::Get()->ExecuteUnsecure($sql) !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        http_response_code(201);
        header("Content-Type: application/json");
        echo "{\"columnId\":".((int)$columnNumber)."}";

        exit(0);
    }
    else if ($action == "read")
    {
        http_response_code(400);
        exit(1);
    }
    else if ($action == "update")
    {
        http_response_code(400);
        exit(1);
    }
    else if ($action == "delete")
    {
        http_response_code(400);
        exit(1);
    }
    else
    {
        /** @todo If this were HTTP verbs, there's HTTP 405. */
        http_response_code(400);
        exit(1);
    }
}
else if ($object == "record")
{
    if ($action == "create")
    {
        require_once("../libraries/column_type_defines.inc.php");

        if (isset($_GET["table-id"]) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        $tableId = (int)$_GET["table-id"];

        if (file_exists("../definitions/table_".$tableId."_column.json") !== true)
        {
            http_response_code(404);
            exit(1);
        }

        $payload = "";

        {
            $source = @fopen("php://input", "r");

            while (true)
            {
                $chunk = @fread($source, 1024);

                if ($chunk == false)
                {
                    break;
                }

                $payload .= $chunk;
            }
        }

        $payload = json_decode($payload, true);

        if ($payload === false)
        {
            http_response_code(400);
            exit(1);
        }

        if (is_array($payload) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        if (count($payload) <= 0)
        {
            http_response_code(400);
            exit(1);
        }

        $columns = @file_get_contents("../definitions/table_".$tableId."_column.json");

        if ($columns === false)
        {
            http_response_code(500);
            exit(-1);
        }

        $columns = json_decode($columns, false);

        if ($columns === null)
        {
            http_response_code(500);
            exit(-1);
        }

        {
            $temp = array();

            foreach ($columns as $column)
            {
                if (isset($temp[(int)$column->id]) === true)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $temp[(int)$column->id] = $column;
            }

            $columns = $temp;
        }


        require_once("../libraries/database.inc.php");

        if (Database::Get()->IsConnected() !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        $recordIds = array();

        foreach ($payload as $record)
        {
            if (is_array($record) !== true)
            {
                http_response_code(400);
                exit(1);
            }

            $columnMapping = array();

            /** @todo Check if there's more values supplied than columns. */
            foreach ($columns as $id => $column)
            {
                $columnMapping[$id] = false;
            }

            foreach ($record as $value)
            {
                if (isset($value["columnId"]) !== true)
                {
                    http_response_code(400);
                    exit(1);
                }

                if (isset($value["value"]) !== true)
                {
                    http_response_code(400);
                    exit(1);
                }

                if (isset($columnMapping[(int)$value["columnId"]]) !== true)
                {
                    http_response_code(400);
                    exit(1);
                }

                if ($columnMapping[(int)$value["columnId"]] === false)
                {
                    $columnMapping[(int)$value["columnId"]]["column"] = $columns[(int)$value["columnId"]];
                    $columnMapping[(int)$value["columnId"]]["value"] = $value;
                }
                else
                {
                    http_response_code(400);
                    exit(1);
                }
            }

            foreach ($columnMapping as $id => $column)
            {
                if ($column === false)
                {
                    http_response_code(400);
                    exit(1);
                }
            }

            $sqlColumns = "`id`";
            $sqlMarks = "?";
            $valuesList = array(NULL);
            $typesList = array(Database::TYPE_NULL);

            foreach ($columnMapping as $id => $data)
            {
                if ($data === null)
                {
                    http_response_code(400);
                    exit(1);
                }

                $sqlColumns .= ", `column_".$id."`";
                $sqlMarks .= ", ?";

                switch ((int)$data["column"]->type)
                {
                case COLUMN_TYPE_VARCHAR:
                    $valuesList[] = $data["value"]["value"];
                    $typesList[] = Database::TYPE_STRING;
                    break;
                case COLUMN_TYPE_BOOLEAN:
                    $valuesList[] = ($data["value"]["value"] === false ? 0 : 1);
                    $typesList[] = Database::TYPE_INT;
                    break;
                case COLUMN_TYPE_INTEGER:
                    $valuesList[] = (int)$data["value"]["value"];
                    $typesList[] = Database::TYPE_INT;
                    break;
                default:
                    http_response_code(500);
                    exit(-1);
                }
            }

            $sql = "INSERT INTO `".Database::Get()->GetPrefix()."table_".$tableId."` (".
                   $sqlColumns.")\n".
                   "VALUES (".
                   $sqlMarks.")";

            $recordId = Database::Get()->Insert($sql,
                                                $valuesList,
                                                $typesList);

            if ($recordId <= 0)
            {
                http_response_code(400);
                exit(1);
            }

            $recordIds[] = $recordId;
        }

        if (count($recordIds) > 0)
        {
            http_response_code(201);
            header("Content-Type: application/json");
            echo "[";

            $first = true;

            foreach ($recordIds as $recordId)
            {
                if ($first == true)
                {
                    $first = false;
                }
                else
                {
                    echo ",";
                }

                echo "{\"recordId\":".((int)$recordId)."}";
            }

            echo "]";
        }
        else
        {
            http_response_code(500);
            exit(-1);
        }
    }
    else if ($action == "read")
    {
        http_response_code(400);
        exit(1);
    }
    else if ($action == "update")
    {
        /** @todo Maybe add support for updating several records at a time
          * (different ones, and/or same in an order?) */

        require_once("../libraries/column_type_defines.inc.php");

        if (isset($_GET["table-id"]) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        $tableId = (int)$_GET["table-id"];

        if (file_exists("../definitions/table_".$tableId."_column.json") !== true)
        {
            http_response_code(404);
            exit(1);
        }

        if (isset($_GET["record-id"]) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        $recordId = (int)$_GET["record-id"];

        $payload = "";

        {
            $source = @fopen("php://input", "r");

            while (true)
            {
                $chunk = @fread($source, 1024);

                if ($chunk == false)
                {
                    break;
                }

                $payload .= $chunk;
            }
        }

        $payload = json_decode($payload, true);

        if ($payload === false)
        {
            http_response_code(400);
            exit(1);
        }

        if (is_array($payload) !== true)
        {
            http_response_code(400);
            exit(1);
        }

        if (count($payload) <= 0)
        {
            http_response_code(400);
            exit(1);
        }

        $columns = @file_get_contents("../definitions/table_".$tableId."_column.json");

        if ($columns === false)
        {
            http_response_code(500);
            exit(-1);
        }

        $columns = json_decode($columns, false);

        if ($columns === null)
        {
            http_response_code(500);
            exit(-1);
        }

        {
            $temp = array();

            foreach ($columns as $column)
            {
                if (isset($temp[(int)$column->id]) === true)
                {
                    http_response_code(500);
                    exit(-1);
                }

                $temp[(int)$column->id] = $column;
            }

            $columns = $temp;
        }


        require_once("../libraries/database.inc.php");

        if (Database::Get()->IsConnected() !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        // This may not be necessariy - only there to check for 404?
        $record = Database::Get()->QueryUnsecure("SELECT *\n".
                                                 "FROM `".Database::Get()->GetPrefix()."table_".((int)$tableId)."`\n".
                                                 "WHERE `id`=".$recordId."\n".
                                                 "LIMIT 1");

        if (is_array($record) !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        $recordCount = count($record);

        if ($recordCount <= 0)
        {
            http_response_code(404);
            exit(-1);
        }

        if ($recordCount > 1)
        {
            http_response_code(500);
            exit(-1);
        }


        $record = $record[0];

        $columnValues = array();

        foreach ($payload as $value)
        {
            if (isset($value["columnId"]) !== true)
            {
                http_response_code(400);
                exit(1);
            }

            $columnId = (int)$value["columnId"];

            if (isset($value["value"]) !== true)
            {
                http_response_code(400);
                exit(1);
            }

            if (isset($columns[$columnId]) !== true)
            {
                http_response_code(400);
                exit(1);
            }

            if (isset($columnValues["column_".$columnId]) !== true)
            {
                $columnValues["column_".$columnId]["value"] = $value["value"];
                $columnValues["column_".$columnId]["column"] = $columns[$columnId];
            }
            else
            {
                http_response_code(400);
                exit(1);
            }
        }

        if (empty($columnValues) === true)
        {
            http_response_code(400);
            exit(0);
        }


        {
            $sqlColumns = "";
            $valuesList = array();
            $typesList = array();

            $first = true;

            foreach ($columnValues as $columnId => $data)
            {
                if ($first == true)
                {
                    $first = false;
                }
                else
                {
                    $sqlColumns .= ", ";
                }

                $sqlColumns .= "`".$columnId."`=?";

                switch ((int)$data["column"]->type)
                {
                case COLUMN_TYPE_VARCHAR:
                    $valuesList[] = $data["value"];
                    $typesList[] = Database::TYPE_STRING;
                    break;
                case COLUMN_TYPE_BOOLEAN:
                    $valuesList[] = ($data["value"] === false ? 0 : 1);
                    $typesList[] = Database::TYPE_INT;
                    break;
                case COLUMN_TYPE_INTEGER:
                    $valuesList[] = (int)$data["value"];
                    $typesList[] = Database::TYPE_INT;
                    break;
                default:
                    http_response_code(500);
                    exit(-1);
                }
            }

            $sql = "UPDATE `".Database::Get()->GetPrefix()."table_".$tableId."`\n".
                   "SET ".$sqlColumns."\n".
                   "WHERE `id`=".$recordId;

            $result = Database::Get()->Execute($sql,
                                               $valuesList,
                                               $typesList);

            if ($result !== true)
            {
                http_response_code(500);
                exit(1);
            }

            http_response_code(200);

            exit(0);
        }
    }
    else if ($action == "delete")
    {
        http_response_code(400);
        exit(1);
    }
    else
    {
        /** @todo If this were HTTP verbs, there's HTTP 405. */
        http_response_code(400);
        exit(1);
    }
}
else
{
    http_response_code(404);
    exit(1);
}
