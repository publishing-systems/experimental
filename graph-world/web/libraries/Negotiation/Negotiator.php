<?php
/* Copyright (C) 2013-2021 William Durand, Stephan Kreutzer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Negotiation;

class Negotiator extends AbstractNegotiator
{
    /**
     * {@inheritdoc}
     */
    protected function acceptFactory($accept)
    {
        return new Accept($accept);
    }

    /**
     * {@inheritdoc}
     */
    protected function match(AcceptHeader $accept, AcceptHeader $priority, $index)
    {
        if (!$accept instanceof Accept || !$priority instanceof Accept) {
            return null;
        }

        $acceptBase = $accept->getBasePart();
        $priorityBase = $priority->getBasePart();

        $acceptSub = $accept->getSubPart();
        $prioritySub = $priority->getSubPart();

        $intersection = array_intersect_assoc($accept->getParameters(), $priority->getParameters());

        $baseEqual = !strcasecmp($acceptBase, $priorityBase);
        $subEqual  = !strcasecmp($acceptSub, $prioritySub);

        if (($acceptBase === '*' || $baseEqual)
            && ($acceptSub === '*' || $subEqual)
            && count($intersection) === count($accept->getParameters())
        ) {
            $score = 100 * $baseEqual + 10 * $subEqual + count($intersection);

            return new AcceptMatch($accept->getQuality() * $priority->getQuality(), $score, $index);
        }

        if (!strstr($acceptSub, '+') || !strstr($prioritySub, '+')) {
            return null;
        }

        // Handle "+" segment wildcards
        $acceptSub = $this->splitSubPart($acceptSub);
        $acceptPlus = $acceptSub[1];
        $acceptSub = $acceptSub[0];
        $prioritySub = $this->splitSubPart($prioritySub);
        $priorityPlus = $prioritySub[1];
        $prioritySub = $prioritySub[0];

        // If no wildcards in either the subtype or + segment, do nothing.
        if (!($acceptBase === '*' || $baseEqual)
            || !($acceptSub === '*' || $prioritySub === '*' || $acceptPlus === '*' || $priorityPlus === '*')
        ) {
            return null;
        }

        $subEqual  = !strcasecmp($acceptSub, $prioritySub);
        $plusEqual = !strcasecmp($acceptPlus, $priorityPlus);

        if (($acceptSub === '*' || $prioritySub === '*' || $subEqual)
            && ($acceptPlus === '*' || $priorityPlus === '*' || $plusEqual)
            && count($intersection) === count($accept->getParameters())
        ) {
            $score = 100 * $baseEqual + 10 * $subEqual + $plusEqual + count($intersection);

            return new AcceptMatch($accept->getQuality() * $priority->getQuality(), $score, $index);
        }

        return null;
    }

    /**
     * Split a subpart into the subpart and "plus" part.
     *
     * For media-types of the form "application/vnd.example+json", matching
     * should allow wildcards for either the portion before the "+" or
     * after. This method splits the subpart to allow such matching.
     */
    protected function splitSubPart($subPart)
    {
        if (!strstr($subPart, '+')) {
            return [$subPart, ''];
        }

        return explode('+', $subPart, 2);
    }
}
