<?php
/* Copyright (C) 2016-2024 Stephan Kreutzer
 *
 * This file is part of graph-world.
 *
 * graph-world is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * graph-world is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with graph-world. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/negotiation.inc.php
 * @details Requires >= PHP 5.3 at the very minimum for
 *     namespaces used for autoload.
 * @todo Update $/web/libraries/Negotiation from
 *     https://github.com/willdurand/Negotiation
 *     (look for the branch that's the current
 *     stable release, but also diff/keep the
 *     changes made to this copy). Current
 *     version based on 3.0.0 (2020-11-26).
 * @author Stephan Kreutzer
 * @since 2016-09-23
 */


function autoload($class_name)
{
    $path = dirname(__FILE__)."/".$class_name.".php";
    $path = str_replace("\\", "/", $path);
    require $path;
}

spl_autoload_register("autoload");


/*
define("CONTENT_TYPE_SUPPORTED_XML", "application/xml; charset=utf-8; q=0.9");
define("CONTENT_TYPE_SUPPORTED_NCX", "application/x-dtbncx+xml; charset=utf-8; q=0.9");
define("CONTENT_TYPE_SUPPORTED_RSS", "application/rss+xml; charset=utf-8; q=0.9");
*/
define("CONTENT_TYPE_SUPPORTED_XHTML", "application/xhtml+xml; charset=utf-8; q=0.8");
define("CONTENT_TYPE_SUPPORTED_JSON", "application/json; charset=utf-8; q=0.7");


function NegotiateContentType($supportedContentTypes)
{
    $acceptHeaderSuggestion = "";

    foreach ($supportedContentTypes as $acceptHeader)
    {
        if (!empty($acceptHeaderSuggestion))
        {
            $acceptHeaderSuggestion .= ",";
        }

        $acceptHeaderSuggestion .= $acceptHeader;
    }

    define("CONTENT_TYPE_SUPPORTED_ACCEPTHEADERSUGGESTION", $acceptHeaderSuggestion);

    $requestedContentTypes = "";

    if (isset($_GET['format']) === true)
    {
        switch ($_GET['format'])
        {
        case "xhtml":
            $requestedContentTypes = CONTENT_TYPE_SUPPORTED_XHTML;
            break;
        case "json":
            $requestedContentTypes = CONTENT_TYPE_SUPPORTED_JSON;
            break;
        /*
        case "xml":
            $requestedContentTypes = CONTENT_TYPE_SUPPORTED_XML;
            break;
        case "ncx":
            $requestedContentTypes = CONTENT_TYPE_SUPPORTED_NCX;
            break;
        case "rss":
            $requestedContentTypes = CONTENT_TYPE_SUPPORTED_RSS;
            break;
        */
        }
    }
    else
    {
        if (isset($_SERVER['HTTP_ACCEPT']) === true)
        {
            $requestedContentTypes = $_SERVER['HTTP_ACCEPT'];
        }
    }

    $mediaType = null;

    if (empty($requestedContentTypes) === false)
    {
        $negotiator = new \Negotiation\Negotiator();

        $mediaType = $negotiator->getBest($requestedContentTypes, $supportedContentTypes);

        if ($mediaType != null)
        {
            $mediaType = $mediaType->getValue();

        }
    }

    if ($mediaType == null)
    {
        http_response_code(406);
        echo CONTENT_TYPE_SUPPORTED_ACCEPTHEADERSUGGESTION;
        exit(-1);
    }

    define("CONTENT_TYPE_REQUESTED", $mediaType);
    header("Content-Type: ".CONTENT_TYPE_REQUESTED);
}

function NegotiateLanguage($acceptLanguageHeader, $languagePriorities)
{
    $negotiator = new \Negotiation\LanguageNegotiator();
    $language = $negotiator->getBest($acceptLanguageHeader, $languagePriorities);

    if ($language != null)
    {
        return $language->getType();
    }

    return null;
}



?>
