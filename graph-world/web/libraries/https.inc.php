<?php
/* Copyright (C) 2016-2024 Stephan Kreutzer
 *
 * This file is part of graph-world.
 *
 * graph-world is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * graph-world is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with graph-world. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/https.inc.php
 * @author Stephan Kreutzer
 * @since 2016-10-23
 */



if (isset($_SERVER['HTTPS']) === true)
{
    if ($_SERVER['HTTPS'] === "on")
    {
        define("HTTPS_ENABLED", true);
    }
    else
    {
        define("HTTPS_ENABLED", false);
    }
}
else
{
    define("HTTPS_ENABLED", false);
}

if (HTTPS_ENABLED !== true)
{
    // HTTPS is required as HTTP Basic authentication relies on encryption.

    header("Location: https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], true, 302);
    exit(-1);
}



?>
