<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-01-23
 */

$sourceUrl = "https://groupware-systems.org/data-federation/data-entry/pattern_select.php?id_template=6";

/** @todo This loads all the data into memory! */
$resource = file_get_contents($sourceUrl."&format=ncx");
$ncx = new SimpleXMLElement($resource);

$lists = array();

foreach ($ncx->children("ncx", true)->navMap->children("ncx", true)->navPoint as $navPoint)
{
    $book = file_get_contents("https://groupware-systems.org/data-federation/data-entry/".$navPoint->children("ncx", true)->content->attributes()["src"]."&format=xml");
    $book = new SimpleXMLElement($book);

    if (array_key_exists($sourceUrl."/".$book->creator, $lists) != true)
    {
        $lists[$sourceUrl."/".$book->creator] = array();
    }

    $lists[$sourceUrl."/".$book->creator][] = $book;
}


header("Content-Type: application/json");

echo "{".
         "\"name\":\"\",".
         "\"url\":".json_encode($sourceUrl).",".
         "\"lists\":[";

$first = true;

foreach ($lists as $url => $books)
{
    if ($first == true)
    {
        $first = false;
    }
    else
    {
        echo ",";
    }

    echo "{\"name\":".json_encode($url).",".
         "\"url\":".json_encode($url).",".
         "\"books\":[";

    for ($i = 0, $max = count($books); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo "{\"title\":".json_encode((string)$books[$i]->title).",".
             "\"id\":".(strlen((string)$books[$i]->id) > 0 ? json_encode((string)$books[$i]->id) : "null").",".
             "\"author\":[";

        for ($j = 0, $max2 = count($books[$i]->authors->author); $j < $max2; $j++)
        {
            if ($j > 0)
            {
                echo ",";
            }

            echo json_encode((string)$books[$i]->authors->author[$j]);
        }

        echo "],".
             "\"link\":".json_encode((string)$books[$i]->link).",".
             "\"image\":".(strlen((string)$books[$i]->image) > 0 ? json_encode((string)$books[$i]->image) : "null").",".
             "\"date_finished\":".(strlen((string)$books[$i]->date_finished) > 0 ? json_encode((string)$books[$i]->date_finished) : "null").",".
             "\"notes\": null}";
    }

    echo "]}";
}

echo     "]".
     "}";


?>
