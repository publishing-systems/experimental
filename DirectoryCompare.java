/* Copyright (C) 2024 Stephan Kreutzer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @since 2024-02-14
 * @author Stephan Kreutzer
 */


import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.io.FileNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


class DirectoryCompare
{
    public static void main(String[] args)
    {
        if (args.length < 2)
        {
            System.out.println("DirectoryCompare <lhs-directory-path> <rhs-directory-path>");
            System.exit(1);
        }

        DirectoryCompare instance = new DirectoryCompare();
        instance.run(args);
    }

    public int run(String[] args)
    {
        File resourceLhs = new File(args[0]);
        File resourceRhs = new File(args[1]);

        try
        {
            resourceLhs = resourceLhs.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            System.out.println(ex.getMessage());
            System.exit(-1);
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
            System.exit(-1);
        }

        if (resourceLhs.exists() != true)
        {
            System.out.println("LHS \"" + resourceLhs.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (resourceLhs.isDirectory() != true)
        {
            System.out.println("LHS \"" + resourceLhs.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        try
        {
            resourceRhs = resourceRhs.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            System.out.println(ex.getMessage());
            System.exit(-1);
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
            System.exit(-1);
        }

        if (resourceRhs.exists() != true)
        {
            System.out.println("RHS \"" + resourceRhs.getAbsolutePath() + "\" doesn't exist.");
            return 1;
        }

        if (resourceRhs.isDirectory() != true)
        {
            System.out.println("RHS \"" + resourceRhs.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        this.compare(resourceLhs, resourceRhs);

        return 0;
    }

    public int compare(File resourceLhs, File resourceRhs)
    {
        File[] childrenLhs = resourceLhs.listFiles();
        File[] childrenRhs = resourceRhs.listFiles();

        Map<String, File> childrenNamesLhs = new HashMap<String, File>();
        Map<String, File> childrenNamesRhs = new HashMap<String, File>();

        for (int i = 0, max = childrenLhs.length; i < max; i++)
        {
            childrenNamesLhs.put(childrenLhs[i].getName(), childrenLhs[i]);
        }

        for (int i = 0, max = childrenRhs.length; i < max; i++)
        {
            childrenNamesRhs.put(childrenRhs[i].getName(), childrenRhs[i]);
        }

        for (Map.Entry<String, File> child : childrenNamesRhs.entrySet())
        {
            if (childrenNamesLhs.containsKey(child.getKey()) != true)
            {
                System.out.println("RHS \"" + child.getValue().getAbsolutePath() + "\" not in LHS \"" + resourceLhs.getAbsolutePath()+ "\".");
            }
        }

        for (Map.Entry<String, File> child : childrenNamesLhs.entrySet())
        {
            if (childrenNamesRhs.containsKey(child.getKey()) != true)
            {
                System.out.println("LHS \"" + child.getValue().getAbsolutePath() + "\" not in RHS \"" + resourceRhs.getAbsolutePath() + "\".");
            }
            else
            {
                File childLhs = child.getValue();
                File childRhs = childrenNamesRhs.get(child.getKey());

                if (childLhs.isDirectory() && childRhs.isDirectory())
                {
                    this.compare(childLhs, childRhs);
                }
                else if (childLhs.isFile() && childRhs.isFile())
                {
                    byte[] hashLhs = null;

                    try
                    {
                        InputStream fileStream = new BufferedInputStream(
                                                 new FileInputStream(childLhs));

                        MessageDigest digest= MessageDigest.getInstance("SHA-256");

                        byte[] buffer = new byte[1024];
                        int bytesRead = fileStream.read(buffer);

                        while (bytesRead > 0)
                        {
                            digest.update(buffer, 0, bytesRead);
                            bytesRead = fileStream.read(buffer);
                        }

                        fileStream.close();

                        hashLhs = digest.digest();
                    }
                    catch (FileNotFoundException ex)
                    {
                        ex.printStackTrace();
                        System.exit(-1);
                    }
                    catch (NoSuchAlgorithmException ex)
                    {
                        ex.printStackTrace();
                        System.exit(-1);
                    }
                    catch (IOException ex)
                    {
                        ex.printStackTrace();
                        System.exit(-1);
                    }

                    byte[] hashRhs = null;

                    try
                    {
                        InputStream fileStream = new BufferedInputStream(
                                                 new FileInputStream(childRhs));

                        MessageDigest digest= MessageDigest.getInstance("SHA-256");

                        byte[] buffer = new byte[1024];
                        int bytesRead = fileStream.read(buffer);

                        while (bytesRead > 0)
                        {
                            digest.update(buffer, 0, bytesRead);
                            bytesRead = fileStream.read(buffer);
                        }

                        fileStream.close();

                        hashRhs = digest.digest();
                    }
                    catch (FileNotFoundException ex)
                    {
                        ex.printStackTrace();
                        System.exit(-1);
                    }
                    catch (NoSuchAlgorithmException ex)
                    {
                        ex.printStackTrace();
                        System.exit(-1);
                    }
                    catch (IOException ex)
                    {
                        ex.printStackTrace();
                        System.exit(-1);
                    }

                    /*
                    for (int i = 0, max = hashLhs.length; i < max; i++)
                    {
                        System.out.print(hashLhs[i]);
                    }

                    System.out.print(" vs. ");

                    for (int i = 0, max = hashRhs.length; i < max; i++)
                    {
                        System.out.print(hashRhs[i]);
                    }

                    System.out.print("\n");
                    */

                    if (hashLhs == null ||
                        Arrays.equals(hashLhs, hashRhs) != true)
                    {
                        System.out.println("LHS \"" + childLhs.getAbsolutePath() + "\" different from RHS \"" + childRhs.getAbsolutePath() + "\".");
                    }
                }
                else
                {
                    System.out.println("LHS \"" + childLhs.getAbsolutePath() + "\" vs. RHS \"" + childRhs.getAbsolutePath() + "\" type mismatch.");
                }
            }
        }

        return 0;
    }
}
