<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-matcher-1.
 *
 * entry-matcher-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-matcher-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-matcher-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/employee_city.php
 * @author Stephan Kreutzer
 * @since 2023-01-26
 */


require_once("../libraries/database.inc.php");

if ($_SERVER['REQUEST_METHOD'] === "GET")
{
    $employee_city = Database::Get()->query("SELECT `id_employee`,\n".
                                            "    `id_city`\n".
                                            "FROM `".Database::GetPrefix()."employee_city`\n".
                                            "WHERE 1\n");

    if ($employee_city == false)
    {
        http_response_code(500);
        exit(1);
    }

    $employee_city = Database::GetResultAssoc($employee_city);

    http_response_code(200);
    header("Content-Type: application/json");

    echo "[";

    for ($i = 0, $max = count($employee_city); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo "{\"id_employee\":".((int)$employee_city[$i]['id_employee']).",".
             "\"id_city\":".((int)$employee_city[$i]['id_city'])."}";
    }

    echo "]";

    exit(0);
}
else if ($_SERVER['REQUEST_METHOD'] === "PUT")
{
    $payload = "";

    {
        $source = @fopen("php://input", "r");

        while (true)
        {
            $chunk = @fread($source, 1024);

            if ($chunk == false)
            {
                break;
            }

            $payload .= $chunk;
        }
    }

    $payload = json_decode($payload, true);

    if ($payload === false)
    {
        http_response_code(400);
        exit(0);
    }

    if (is_array($payload) != true)
    {
        http_response_code(400);
        exit(0);
    }

    if (isset($payload['cities']) != true)
    {
        http_response_code(400);
        echo "'cities' is missing.";
        exit(0);
    }

    $cities = $payload['cities'];

    if (count($cities) <= 0)
    {
        http_response_code(400);
        echo "'cities' is empty.";
        exit(0);
    }

    $mapping = array();

    for ($i = 0, $max = count($cities); $i < $max; $i++)
    {
        $city = $cities[$i];

        if (is_array($city) !== true)
        {
            http_response_code(400);
            echo "'city' isn't an associative array.";
            exit(0);
        }

        if (isset($city['id']) !== true)
        {
            http_response_code(400);
            echo "'city' without 'id'.";
            exit(0);
        }

        if (isset($city['employees']) !== true)
        {
            http_response_code(400);
            echo "'city' without 'employees'.";
            exit(0);
        }

        if (is_array($city['employees']) !== true)
        {
            http_response_code(400);
            echo "'employees' isn't an array.";
            exit(0);
        }

        for ($j = 0, $max2 = count($city['employees']); $j < $max2; $j++)
        {
            $employee = $city['employees'][$j];

            if (is_array($employee) !== true)
            {
                http_response_code(400);
                echo "'employee' isn't an associative array.";
                exit(0);
            }

            if (isset($employee['id']) !== true)
            {
                http_response_code(400);
                echo "'employee' without 'id'.";
                exit(0);
            }

            if (array_key_exists((int)$city['id'], $mapping) != true)
            {
                $mapping[(int)$city['id']] = array();
            }

            $mapping[(int)$city['id']][] = (int)$employee['id'];
        }
    }

    {
        $citiesUnique = array();
        $employeesUnique = array();

        foreach ($mapping as $cityId => $employees)
        {
            for ($i = 0, $max = count($employees); $i < $max; $i++)
            {
                if (in_array($employees[$i], $employeesUnique) == true)
                {
                    http_response_code(400);
                    echo "Employee ID within two different cities.";
                    exit(0);
                }
                else
                {
                    $employeesUnique[] = $employees[$i];
                }
            }

            $citiesUnique[] = $cityId;
        }

        $citiesDb = Database::Get()->query("SELECT `id`\n".
                                           "FROM `".Database::GetPrefix()."city`\n".
                                           "WHERE 1\n");

        if ($citiesDb == false)
        {
            http_response_code(500);
            exit(1);
        }

        $citiesDb = Database::GetResultAssoc($citiesDb);

        for ($i = 0, $max = count($citiesDb); $i < $max; $i++)
        {
            $citiesDb[$i] = (int)$citiesDb[$i]['id'];
        }

        for ($i = 0, $max = count($citiesUnique); $i < $max; $i++)
        {
            if (in_array($citiesUnique[$i], $citiesDb) !== true)
            {
                http_response_code(400);
                echo "City with ID '".($citiesUnique[$i])."' is missing in the database.";
                exit(0);
            }
        }


        $employeesDb = Database::Get()->query("SELECT `id`\n".
                                              "FROM `".Database::GetPrefix()."employee`\n".
                                              "WHERE 1\n");

        if ($employeesDb == false)
        {
            http_response_code(500);
            exit(1);
        }

        $employeesDb = Database::GetResultAssoc($employeesDb);

        for ($i = 0, $max = count($employeesDb); $i < $max; $i++)
        {
            $employeesDb[$i] = (int)$employeesDb[$i]['id'];

            $index = array_search($employeesDb[$i], $employeesUnique);

            if ($index === false)
            {
                http_response_code(400);
                echo "Employee with ID '".($employeesDb[$i])."' is missing.";
                exit(0);
            }

            unset($employeesUnique[$index]);
        }

        if (count($employeesUnique) != 0)
        {
            http_response_code(400);
            echo "Employee missing in the database.";
            exit(0);
        }
    }

    if (Database::Get()->begin_transaction() !== true)
    {
        http_response_code(500);
        exit(1);
    }
    
    $result = Database::Get()->query("DELETE FROM `".Database::GetPrefix()."employee_city`\n".
                                     "WHERE 1\n");

    if ($result !== true)
    {
        Database::Get()->rollback();
        http_response_code(500);
        exit(1);
    }

    $query = "";

    foreach ($mapping as $cityId => $employees)
    {
        for ($i = 0, $max = count($employees); $i < $max; $i++)
        {
            if (strlen($query) > 0)
            {
                $query .= ",\n    ";
            }

            $query .= "(".$employees[$i].", ".$cityId.")";
        }
    }

    $result = Database::Get()->query("INSERT INTO `".Database::GetPrefix()."employee_city` (`id_employee`,\n".
                                     "    `id_city`)\n".
                                     "VALUES\n".
                                     $query);

    if ($result !== true)
    {
        Database::Get()->rollback();
        http_response_code(500);
        exit(1);
    }

    if (Database::Get()->commit() !== true)
    {
        Database::Get()->rollback();
        http_response_code(500);
        exit(1);
    }
    if (Database::Get()->insert_id === NULL)
    {
        return -1;
    }

    http_response_code(204);
}
else
{
    http_response_code(405);
    exit(0);
}


?>
