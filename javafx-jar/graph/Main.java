/* Copyright (C) 2024 Stephan Kreutzer
 *
 * This file is part of Graph.
 *
 * Graph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Graph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Graph. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Main.java
 * @author Stephan Kreutzer
 * @since 2024-09-01
 */

// https://github.com/javafxports/openjdk-jfx/issues/236#issuecomment-426583174
// If the class with the main method is inherited from JavaFX Application,
// in order to instantiate/execute, the Java VM will require the installed
// JavaFX module to be loaded, resulting in "Error: JavaFX runtime components
// are missing, and are required to run this application" if such is not
// referenced in the module path (without further or more detailed explanation).
// To post-load JavaFX as JAR modules, a class with the main function not
// derived from JavaFX classes is needed, which then is sufficient to load
// other JavaFX dependencies as local JAR files.
public class Main
{
    public static void main(String[] args)
    {
        System.out.print("Graph Copyright (C) 2022-2024 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/experimental/ and\n" +
                         "the project website https://hypertext-systems.org.\n\n");

        App app = new App();
        app.run(args);
    }
}
