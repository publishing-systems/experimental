/* Copyright (C) 2022-2024 Stephan Kreutzer
 *
 * This file is part of Graph.
 *
 * Graph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Graph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Graph. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/App.java
 * @details Inspired by https://github.com/einfachuwe42/uwe-javafx-canvas
 * @author Stephan Kreutzer
 * @since 2022-04-19
 */

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import java.util.Map;
import java.util.List;

public class App extends Application
{
    public App()
    {

    }

    public int run(String[] args)
    {
        launch(args);
        return 0;
    }

    public void start(Stage stagePrimary)
    {
        stagePrimary.setTitle("Graph");

        FlowPane rootNode = new FlowPane();

        GraphView graphView = new GraphView(rootNode, 500.0, 400.0);

        /*
        Map.Entry<List<GraphNode>, List<GraphEdge>> circle = GraphView.generateCircle(100, 100, 75, 12, graphView.getCurrentNodeSize());

        for (int i = 0, max = circle.getKey().size(); i < max; i++)
        {
            graphView.addNode(circle.getKey().get(i));
        }

        for (int i = 0, max = circle.getValue().size(); i < max; i++)
        {
            graphView.addEdge(circle.getValue().get(i));
        }
        */

        Scene scene = new Scene(rootNode);
        stagePrimary.setScene(scene);
        stagePrimary.show();
    }
}
