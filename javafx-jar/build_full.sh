#!/bin/sh

# sudo apt-get install default-jdk openjfx

rm ./graph/*.class
javac -encoding UTF-8 --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls ./graph/*.java


# https://docs.oracle.com/javase/tutorial/deployment/jar/downman.html
# According to the note, other JARs packaged into the JAR cannot be referenced
# per the manifest Class-Path as a dependency, only other external local
# JARs/modules. This is likely by design, to make use of installed dependencies
# instead of bundling many duplicates with every application/fat-JAR. To
# support loading of "embedded" JARs in the JAR, this is likely possible by
# writing a custom dynamic loader.
#jar cfm graph.jar ./manifest_full.txt -C ./graph/ . -C /usr/share/openjfx/lib/ javafx.base.jar -C /usr/share/openjfx/lib/ javafx.graphics.jar

jar cfm graph.jar ./manifest_full.txt -C ./graph/ .

cp /usr/share/openjfx/lib/javafx.base.jar .
cp /usr/share/openjfx/lib/javafx.graphics.jar .

cp /usr/share/openjfx/lib/javafx.controls.jar .
cp /usr/share/openjfx/lib/javafx.fxml.jar .
cp /usr/share/openjfx/lib/javafx.media.jar .
cp /usr/share/openjfx/lib/javafx.swing.jar .
cp /usr/share/openjfx/lib/javafx.web.jar .
