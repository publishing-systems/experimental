/* Copyright (C) 2022-2024 Stephan Kreutzer
 *
 * This file is part of Graph.
 *
 * Graph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Graph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Graph. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/GraphNode.java
 * @todo Node doesn't know about connected edges.
 * @author Stephan Kreutzer
 * @since 2022-04-11
 */

public class GraphNode
{
    public GraphNode(int x, int y, double size)
    {
        this.x = x;
        this.y = y;
        this.size = size;
        this.dirty = true;
    }

    public int getX()
    {
        return this.x;
    }

    public int setX(int x)
    {
        this.x = x;
        this.dirty = true;

        return 0;
    }

    public int getY()
    {
        return this.y;
    }

    public int setY(int y)
    {
        this.y = y;
        this.dirty = true;

        return 0;
    }

    public double getSize()
    {
        return this.size;
    }

    public int setSize(double size)
    {
        this.size = size;
        return 0;
    }

    public boolean getDirty()
    {
        return this.dirty;
    }

    public int setDirty(boolean dirty)
    {
        this.dirty = dirty;
        return 0;
    }

    public boolean getSelected()
    {
        return this.selected;
    }

    public int setSelected(boolean selected)
    {
        if (this.selected != selected)
        {
            this.selected = selected;
            this.dirty = true;

            return 1;
        }
        else
        {
            return 0;
        }
    }

    public boolean getDoubleClicked()
    {
        return this.doubleClicked;
    }

    public int setDoubleClicked(boolean doubleClicked)
    {
        if (this.doubleClicked != doubleClicked)
        {
            this.doubleClicked = doubleClicked;
            this.dirty = true;

            return 1;
        }
        else
        {
            return 0;
        }
    }

    protected int x = 1;
    protected int y = 1;
    protected double size = 1.0;
    protected boolean dirty = false;
    protected boolean selected = false;
    protected boolean doubleClicked = false;
}
