/* Copyright (C) 2022-2024 Stephan Kreutzer
 *
 * This file is part of Graph.
 *
 * Graph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Graph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Graph. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/GraphEdge.java
 * @todo No "thickness"/weight to an edge, no directionality (support bi-directionality
 *     as well?), nor does know (or can request) distance between the connected nodes, yet.
 * @author Stephan Kreutzer
 * @since 2022-04-11
 */

public class GraphEdge
{
    public GraphEdge(GraphNode from, GraphNode to)
    {
        this.from = from;
        this.to = to;
        this.dirty = true;
    }

    public GraphNode getFrom()
    {
        return this.from;
    }

    public GraphNode getTo()
    {
        return this.to;
    }

    public boolean getDirty()
    {
        return this.dirty;
    }

    public int setDirty(boolean dirty)
    {
        this.dirty = dirty;
        return 0;
    }

    public boolean getSelected()
    {
        return this.selected;
    }

    protected GraphNode from = null;
    protected GraphNode to = null;
    protected boolean dirty = false;
    protected boolean selected = false;
}
