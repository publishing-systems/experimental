/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of Radar Chart.
 *
 * Radar Chart is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * Radar Chart is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Radar Chart. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function createRadarChart(targetId, size, padding, data)
{
    function createAxis(target, center, dimensionLength, dimensions)
    {
        let groupAxis = document.createElementNS("http://www.w3.org/2000/svg", "g");
        groupAxis.setAttribute("class", "dimension-axis");

        let angle = 0;
        let stepAngle = 360 / dimensions.length;

        for (let i = 0, max = dimensions.length; i < max; i++)
        {
            let radians = (angle + 270) * (Math.PI / 180.0);
            let x = center + (dimensionLength * Math.cos(radians));
            let y = center + (dimensionLength * Math.sin(radians));

            angle += stepAngle;

            let line = document.createElementNS("http://www.w3.org/2000/svg", "path");
            line.setAttribute("class", "dimension");
            line.setAttribute("d", "M " + center + " " + center + " L " + x + " " + y);

            groupAxis.appendChild(line);
        }

        target.appendChild(groupAxis);
    }

    function createLevels(target, center, dimensionLength, dimensions, dimensionMin, dimensionMax)
    {
        let groupLevels = document.createElementNS("http://www.w3.org/2000/svg", "g");
        groupLevels.setAttribute("class", "dimension-level");

        let dimensionSpan = dimensionMax - dimensionMin;

        let levelOffset = dimensionLength / dimensionSpan;

        let angle = 0;
        let stepAngle = 360 / dimensions.length;

        for (let i = 1; i <= dimensionSpan; i++)
        {
            for (let j = 0, max = dimensions.length; j < max; j++)
            {
                let startRadians = (angle + 270) * (Math.PI / 180.0);
                let startX = center + ((levelOffset * i) * Math.cos(startRadians));
                let startY = center + ((levelOffset * i) * Math.sin(startRadians));

                let endRadians = ((angle + stepAngle) + 270) * (Math.PI / 180.0);
                let endX = center + ((levelOffset * i) * Math.cos(endRadians));
                let endY = center + ((levelOffset * i) * Math.sin(endRadians));

                angle += stepAngle;

                let line = document.createElementNS("http://www.w3.org/2000/svg", "path");
                line.setAttribute("class", "level");
                line.setAttribute("d", "M " + startX + " " + startY + " L " + endX + " " + endY);

                groupLevels.appendChild(line);
            }
        }

        target.appendChild(groupLevels);
    }

    function createValues(target, center, dimensionLength, data, dimensionMin, dimensionMax)
    {
        let groupValues = document.createElementNS("http://www.w3.org/2000/svg", "g");
        groupValues.setAttribute("class", "values");

        let dimensionSpan = dimensionMax - dimensionMin;

        let levelOffset = dimensionLength / dimensionSpan;

        for (let i = 0, max = data.values.length; i < max; i++)
        {
            let item = data.values[i];

            if (item.values.length != data.dimensions.length)
            {
                console.log("Mismatch between count of values and number of dimensions.");
                return -1;
            }

            let groupItem = document.createElementNS("http://www.w3.org/2000/svg", "g");
            groupItem.setAttribute("class", "item");

            let angle = 0;
            let stepAngle = 360 / data.dimensions.length;

            for (let j = 0, max2 = item.values.length; j < max2; j++)
            {
                let startRadians = (angle + 270) * (Math.PI / 180.0);
                let startX = center + ((levelOffset * (item.values[j] - dimensionMin)) * Math.cos(startRadians));
                let startY = center + ((levelOffset * (item.values[j] - dimensionMin)) * Math.sin(startRadians));

                let endRadians = ((angle + stepAngle) + 270) * (Math.PI / 180.0);
                let endX = center + ((levelOffset * (item.values[j + 1] - dimensionMin)) * Math.cos(endRadians));
                let endY = center + ((levelOffset * (item.values[j + 1] - dimensionMin)) * Math.sin(endRadians));

                angle += stepAngle;

                let line = document.createElementNS("http://www.w3.org/2000/svg", "path");
                line.setAttribute("class", "value");
                line.setAttribute("d", "M " + startX + " " + startY + " L " + endX + " " + endY);
                line.setAttribute("class", item.class);

                groupItem.appendChild(line);
            }

            {
                angle -= stepAngle;

                let startRadians = (angle + 270) * (Math.PI / 180.0);
                let startX = center + ((levelOffset * (item.values[item.values.length - 1] - dimensionMin)) * Math.cos(startRadians));
                let startY = center + ((levelOffset * (item.values[item.values.length - 1] - dimensionMin)) * Math.sin(startRadians));

                let endRadians = ((angle + stepAngle) + 270) * (Math.PI / 180.0);
                let endX = center + ((levelOffset * (item.values[0] - dimensionMin)) * Math.cos(endRadians));
                let endY = center + ((levelOffset * (item.values[0] - dimensionMin)) * Math.sin(endRadians));



                let line = document.createElementNS("http://www.w3.org/2000/svg", "path");
                line.setAttribute("class", "value");
                line.setAttribute("d", "M " + startX + " " + startY + " L " + endX + " " + endY);
                line.setAttribute("class", item.class);

                groupItem.appendChild(line);
            }

            groupValues.appendChild(groupItem);
        }

        target.appendChild(groupValues);
    }


    let target = document.getElementById(targetId);

    if (target == null)
    {
        console.log("Element with ID '" + target-id + "' not found.");
        return -1;
    }

    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("width", size);
    svg.setAttribute("height", size);

    let dimensionMin = null;
    let dimensionMax = null;

    for (let i = 0, max = data.values.length; i < max; i++)
    {
        for (let j = 0, max2 = data.values[i].values.length; j < max2; j++)
        {
            if (data.values[i].values[j] < dimensionMin ||
                dimensionMin === null)
            {
                dimensionMin = data.values[i].values[j];
            }

            if (data.values[i].values[j] > dimensionMax ||
                dimensionMax === null)
            {
                dimensionMax = data.values[i].values[j];
            }
        }
    }

    let center = size / 2;
    let dimensionLength = center - padding;

    let result = createAxis(svg, center, dimensionLength, data.dimensions);
    result = createLevels(svg, center, dimensionLength, data.dimensions, dimensionMin, dimensionMax);
    result = createValues(svg, center, dimensionLength, data, dimensionMin, dimensionMax);

    target.appendChild(svg);
}
