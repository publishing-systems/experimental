/*
Copyright (C) 2017-2020 Stephan Kreutzer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

"use strict";

function handleRequest(request, response)
{
    if (request.url == "/write")
    {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        let fs = require('fs');

        fs.writeFile("./output.txt", "Hello, world!", function(err) {
            if (err)
            {
                console.log(err);
                response.statusCode = 500;
            }
            else
            {
                console.log("The file was saved!");
                response.statusCode = 200;
            }

            response.end();
        });
    }
    else
    {
        response.statusCode = 404;
        response.end();
    }
}

let http = require('http');
let server = http.createServer(handleRequest);

server.listen(8080, function() { });
