<?php
/* Copyright (C) 2014-2019  Stephan Kreutzer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */



if (isset($_GET['id']) !== true)
{
    header("HTTP/1.0 400 Bad Request");
    exit(0);
}

if ($_GET['id'] !== "2pfweo6Ef415wdHRhsKBxNXY5JM9rVIs")
{
    header("HTTP/1.0 404 Not Found");
    exit(0);
}

$file = "./data/myfile.zip";

if (file_exists($file) !== true)
{
    header("HTTP/1.0 404 Not Found");
    exit(0);
}

header("Content-type:application/zip");
header("Content-Disposition:attachment;filename=myfile.zip");

readfile($file);
unlink($file);

@mail("recipient@example.org",
      "[website] Download",
      "File was downloaded and deleted.",
      "From: noreply@example.org\n".
      "MIME-Version: 1.0\n".
      "Content-type: text/plain; charset=UTF-8\n");

?>
